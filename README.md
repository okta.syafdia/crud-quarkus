# ictcd-crud-core Project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application

Install Docker, and run Docker using:
```shell script
docker-compose up
```
