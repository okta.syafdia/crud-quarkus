package local.okta.crud.delivery.http;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.junit.mockito.InjectSpy;
import io.restassured.http.ContentType;
import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.Post;
import local.okta.crud.domain.usecase.CreateOrUpdatePostUseCase;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import javax.ws.rs.core.Response.Status;

import javax.inject.Inject;

@QuarkusTestResource(H2DatabaseTestResource.class)
public class PostsControllerTest {

    @Test
    public void create_onSuccess_RespondWith201() {
        var post = new Post();
        post.title = "This is post title";
        post.content = "This is post content";

        given()
          .contentType(ContentType.JSON)
          .body(post)
          .when()
          .post("/posts")
          .then()
            .statusCode(Status.CREATED.getStatusCode())
            .body("title", is(post.title))
            .body("content", is(post.content));
    }

    @Test
    public void create_onInvalidBody_respond400() {
      given()
        .contentType(ContentType.JSON)
        .when()
        .post("/posts")
        .then()
          .statusCode(Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    public void update_onInvalidBody_respond400() {
      given()
        .contentType(ContentType.JSON)
        .when()
        .put("/posts/10")
        .then()
          .statusCode(Status.BAD_REQUEST.getStatusCode());
    }
    
}
