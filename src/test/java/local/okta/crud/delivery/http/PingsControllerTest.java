package local.okta.crud.delivery.http;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTestResource(H2DatabaseTestResource.class)
public class PingsControllerTest {

    @Test
    public void ping_onSuccess_respondWith200() {
        given()
          .when()
          .get("/ping")
          .then()
             .statusCode(200)
             .body(is("pong"));
    }

}