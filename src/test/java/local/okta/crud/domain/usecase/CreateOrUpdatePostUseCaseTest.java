package local.okta.crud.domain.usecase;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.google.inject.Inject;

import org.junit.jupiter.api.Test;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import local.okta.crud.domain.entity.Post;
import local.okta.crud.domain.usecase.implementation.DefaultCreateOrUpdatePostUseCase;

@QuarkusTestResource(H2DatabaseTestResource.class)
public class CreateOrUpdatePostUseCaseTest {

    @Inject
    CreateOrUpdatePostUseCase createOrUpdatePostUseCase;

    @Test
    public void execute_onSuccess_returnPost() {
        var post = new Post();
        post.title = "This is post title";
        post.content = "This is post content";

        var result = createOrUpdatePostUseCase.execute(post).await().indefinitely();

        assertFalse(result.id.equals(post.id));
        assertTrue(result.title.equals(post.title));
    }
    
}
