package local.okta.crud.domain.entity;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public class PostResponse {

    public BigInteger id;

    public String title;

    public String content;

    public List<String> tags;

    public String createdAt;

    public Date updatedAt = new Date();

    public PostResponse() {
    }

    public PostResponse(
        BigInteger id,
        String title,
        String content,
        List<String> tags 
    ) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.tags = tags;
    }
    
    
}
