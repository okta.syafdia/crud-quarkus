package local.okta.crud.domain.entity;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public abstract class DataPersistable {
    public BigInteger id;

    public LocalDateTime createdAt;

    public LocalDateTime updatedAt;
}
