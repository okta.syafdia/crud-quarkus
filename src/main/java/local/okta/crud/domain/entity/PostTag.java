package local.okta.crud.domain.entity;

import java.math.BigInteger;

public class PostTag {
    public BigInteger postId;

    public BigInteger tagId;

    public PostTag(BigInteger postId, BigInteger tagId) {
        this.postId = postId;
        this.tagId  = tagId;
    }
}
