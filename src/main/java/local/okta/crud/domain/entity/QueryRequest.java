package local.okta.crud.domain.entity;

import java.math.BigInteger;

public class QueryRequest {
    public Integer offset;

    public Integer limit;

    public String key;

    public Object value;
}
