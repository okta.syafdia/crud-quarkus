package local.okta.crud.domain.usecase.implementation;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;

import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.PostTag;
import local.okta.crud.domain.repository.PostRepository;
import local.okta.crud.domain.usecase.SetPostTagUseCase;
import local.okta.crud.util.Errors;

@ApplicationScoped
public class DefaultSetPostTagUseCase implements SetPostTagUseCase {

    @Inject
    PostRepository postRepository;

    @Override
    public Uni<Void> execute(PostTag postTag) {
        return this.postRepository
            .setTag(postTag.postId, postTag.tagId)
            .onFailure()
            .transform(Errors::catchError)
            .map((isSuccess) -> isSuccess ? Void.TYPE : new NotFoundException("Post or tag is not exist"))
            .replaceWithVoid();
    }
    
}
