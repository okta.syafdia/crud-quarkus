package local.okta.crud.domain.usecase;

import java.math.BigInteger;

import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.Tag;

public interface FindOneTagUseCase extends UseCase<BigInteger, Uni<Tag>> {
    
}
