package local.okta.crud.domain.usecase.implementation;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;

import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.PostTag;
import local.okta.crud.domain.repository.PostRepository;
import local.okta.crud.domain.usecase.SetPostTagUseCase;
import local.okta.crud.domain.usecase.UnsetPostTagUseCase;

@ApplicationScoped
public class DefaultUnsetPostTagUseCase implements UnsetPostTagUseCase {

    @Inject
    PostRepository postRepository;

    @Override
    public Uni<Void> execute(PostTag postTag) {
        return this.postRepository
            .unsetTag(postTag.postId, postTag.tagId)
            .map((isSuccess) -> isSuccess ? Void.TYPE : new NotFoundException("Post or tag is not exist"))
            .replaceWithVoid();
    }
    
}
