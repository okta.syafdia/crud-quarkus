package local.okta.crud.domain.usecase.implementation;

import java.util.NoSuchElementException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;

import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.Post;
import local.okta.crud.domain.entity.Tag;
import local.okta.crud.domain.repository.TagRepository;
import local.okta.crud.domain.usecase.CreateOrUpdateTagUseCase;
import local.okta.crud.util.Errors;

@ApplicationScoped
public class DefaultCreateOrUpdateTagUseCase implements CreateOrUpdateTagUseCase {

    @Inject
    TagRepository tagRepository;

    @Override
    public Uni<Tag> execute(Tag tag) {
        if (tag.id == null) {
            return this.tagRepository.create(tag);
        }

        return this.tagRepository
            .update(tag)
            .onFailure()
            .transform(Errors::catchError);
    }
    
}
