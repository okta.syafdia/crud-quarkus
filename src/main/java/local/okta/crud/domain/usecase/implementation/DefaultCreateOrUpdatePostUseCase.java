package local.okta.crud.domain.usecase.implementation;

import java.util.NoSuchElementException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;

import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.Post;
import local.okta.crud.domain.repository.PostRepository;
import local.okta.crud.domain.usecase.CreateOrUpdatePostUseCase;
import local.okta.crud.util.Errors;

@ApplicationScoped
public class DefaultCreateOrUpdatePostUseCase implements CreateOrUpdatePostUseCase {

    @Inject
    PostRepository postRepository;

    @Override
    public Uni<Post> execute(Post post) {
        if (post.id == null) {
            return this.postRepository.create(post);
        }

        return this.postRepository
            .update(post)
            .onFailure()
            .transform(Errors::catchError);
    }
    
}
