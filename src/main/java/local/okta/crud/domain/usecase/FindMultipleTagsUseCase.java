package local.okta.crud.domain.usecase;

import local.okta.crud.domain.entity.QueryRequest;

import java.util.List;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.Tag;

public interface FindMultipleTagsUseCase extends UseCase<QueryRequest, Uni<List<Tag>>> {
    
}
