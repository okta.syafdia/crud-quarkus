package local.okta.crud.domain.usecase.implementation;

import java.math.BigInteger;
import java.util.List;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.QueryRequest;
import local.okta.crud.domain.entity.Tag;
import local.okta.crud.domain.entity.Post;
import local.okta.crud.domain.repository.PostRepository;
import local.okta.crud.domain.repository.TagRepository;
import local.okta.crud.domain.usecase.FindMultipleTagsUseCase;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class DefaultFindMultipleTagsUseCase implements FindMultipleTagsUseCase {

    @Inject
    TagRepository tagRepository;

    @Override
    public Uni<List<Tag>> execute(QueryRequest query) {
        if (query.key == "post_id") {
            return this.tagRepository.findMultipleByPostId(query.offset, query.limit, (BigInteger) query.value);
        }
        
        return this.tagRepository.find(
            query.offset, 
            query.limit, 
            query.key, 
            query.value
        );
    }
    
}
