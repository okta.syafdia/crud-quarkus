package local.okta.crud.domain.usecase;

import local.okta.crud.domain.entity.QueryRequest;

import java.util.List;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.Post;

public interface FindMultiplePostsUseCase extends UseCase<QueryRequest, Uni<List<Post>>> {}
