package local.okta.crud.domain.usecase.implementation;

import java.math.BigInteger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;

import io.smallrye.mutiny.Uni;
import io.vertx.pgclient.PgException;
import local.okta.crud.domain.repository.PostRepository;
import local.okta.crud.domain.usecase.DeletePostUseCase;
import local.okta.crud.util.Errors;

@ApplicationScoped
public class DefaultDeletePostUseCase implements DeletePostUseCase {

    @Inject
    PostRepository postRepository;

    @Override
    public Uni<Void> execute(BigInteger id) {
        return this.postRepository
            .delete(id)
            .onFailure()
            .transform(Errors::catchError)
            .map((isSuccess) -> isSuccess ? Void.TYPE : new NotFoundException("Post is not exist"))
            .replaceWithVoid();
    }
    
}
