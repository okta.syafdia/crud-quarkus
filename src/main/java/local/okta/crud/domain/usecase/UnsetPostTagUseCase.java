package local.okta.crud.domain.usecase;

import local.okta.crud.domain.entity.PostTag;
import io.smallrye.mutiny.Uni;

public interface UnsetPostTagUseCase extends UseCase<PostTag, Uni<Void>> {
    
}
