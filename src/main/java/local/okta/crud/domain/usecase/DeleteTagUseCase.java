package local.okta.crud.domain.usecase;

import java.math.BigInteger;

import io.smallrye.mutiny.Uni;

public interface DeleteTagUseCase extends UseCase<BigInteger, Uni<Void>> {
    
}
