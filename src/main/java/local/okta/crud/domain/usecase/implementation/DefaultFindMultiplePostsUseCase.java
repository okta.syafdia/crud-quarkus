package local.okta.crud.domain.usecase.implementation;

import java.math.BigInteger;
import java.util.List;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.QueryRequest;
import local.okta.crud.domain.entity.Post;
import local.okta.crud.domain.repository.PostRepository;
import local.okta.crud.domain.usecase.FindMultiplePostsUseCase;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class DefaultFindMultiplePostsUseCase implements FindMultiplePostsUseCase {

    @Inject
    PostRepository postRepository;

    @Override
    public Uni<List<Post>> execute(QueryRequest query) {
        if (query.key == "tag_id") {
            return this.postRepository.findMultipleByTagId(
                query.offset, 
                query.limit, 
                (BigInteger) query.value
            );
        }

        return this.postRepository.find(
            query.offset, 
            query.limit, 
            query.key, 
            query.value
        );
    }
    
}
