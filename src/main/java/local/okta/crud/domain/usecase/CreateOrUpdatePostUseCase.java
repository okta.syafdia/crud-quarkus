package local.okta.crud.domain.usecase;

import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.Post;

public interface CreateOrUpdatePostUseCase extends UseCase<Post, Uni<Post>> {
    
}
