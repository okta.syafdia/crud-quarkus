package local.okta.crud.domain.usecase;

import local.okta.crud.domain.entity.PostTag;
import io.smallrye.mutiny.Uni;

public interface SetPostTagUseCase extends UseCase<PostTag, Uni<Void>> {
    
}
