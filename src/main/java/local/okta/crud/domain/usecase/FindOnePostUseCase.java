package local.okta.crud.domain.usecase;

import java.math.BigInteger;

import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.Post;

public interface FindOnePostUseCase extends UseCase<BigInteger, Uni<Post>> {
    
}
