package local.okta.crud.domain.usecase;

public interface UseCase<T, U> {
    U execute(T t);
}
