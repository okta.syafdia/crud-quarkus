package local.okta.crud.domain.usecase.implementation;

import java.math.BigInteger;
import java.util.List;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.QueryRequest;
import local.okta.crud.domain.entity.Post;
import local.okta.crud.domain.repository.PostRepository;
import local.okta.crud.domain.usecase.FindMultiplePostsUseCase;
import local.okta.crud.domain.usecase.FindOnePostUseCase;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;

@ApplicationScoped
public class DefaultFindOnePostUseCase implements FindOnePostUseCase {

    @Inject
    PostRepository postRepository;

    @Override
    public Uni<Post> execute(BigInteger id) {
        return this.postRepository
        .find(0, 1, "id", id)
        .map((posts) -> {
            if (posts.size() == 0) {
                throw new NotFoundException("Post is not exist");
            }

            return posts.get(0);
        });
    }
    
}
