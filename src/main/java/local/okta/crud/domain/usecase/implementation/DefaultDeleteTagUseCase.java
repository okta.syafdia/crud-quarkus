package local.okta.crud.domain.usecase.implementation;

import java.math.BigInteger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;

import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.repository.TagRepository;
import local.okta.crud.domain.usecase.DeleteTagUseCase;
import local.okta.crud.domain.usecase.UseCase;
import local.okta.crud.util.Errors;

@ApplicationScoped
public class DefaultDeleteTagUseCase implements DeleteTagUseCase {

    @Inject
    TagRepository tagRepository;

    @Override
    public Uni<Void> execute(BigInteger id) {
        return this.tagRepository   
            .delete(id)
            .onFailure()
            .transform(Errors::catchError)
            .map((isSuccess) -> isSuccess ? Void.TYPE : new NotFoundException("Tag is not exist"))
            .replaceWithVoid();
    }
    
}
