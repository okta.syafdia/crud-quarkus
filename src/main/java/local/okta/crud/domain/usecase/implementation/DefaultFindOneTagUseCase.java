package local.okta.crud.domain.usecase.implementation;

import java.math.BigInteger;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.QueryRequest;
import local.okta.crud.domain.entity.Tag;
import local.okta.crud.domain.entity.Post;
import local.okta.crud.domain.repository.PostRepository;
import local.okta.crud.domain.repository.TagRepository;
import local.okta.crud.domain.usecase.FindMultiplePostsUseCase;
import local.okta.crud.domain.usecase.FindOnePostUseCase;
import local.okta.crud.domain.usecase.FindOneTagUseCase;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;

@ApplicationScoped
public class DefaultFindOneTagUseCase implements FindOneTagUseCase {

    @Inject
    TagRepository tagRepository;

    @Override
    public Uni<Tag> execute(BigInteger id) {
        return this.tagRepository
            .find(0, 1, "id", id)
            .map((tags) -> {
                if (tags.size() == 0) {
                    throw new NotFoundException("Tag is not exist");
                }
    
                return tags.get(0);
            });
    }
    
}
