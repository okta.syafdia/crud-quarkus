package local.okta.crud.domain.usecase;

import java.math.BigInteger;

import io.smallrye.mutiny.Uni;

public interface DeletePostUseCase extends UseCase<BigInteger, Uni<Void>> {
    
}
