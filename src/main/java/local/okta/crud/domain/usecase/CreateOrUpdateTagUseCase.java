package local.okta.crud.domain.usecase;

import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.Tag;

public interface CreateOrUpdateTagUseCase extends UseCase<Tag, Uni<Tag>> {
    
}
