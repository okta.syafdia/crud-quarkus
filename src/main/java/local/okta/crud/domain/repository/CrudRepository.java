package local.okta.crud.domain.repository;

import java.math.BigInteger;
import java.util.List;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;

public interface CrudRepository<T> {
    Uni<T> create(T t);

    Uni<T> update(T t);

    Uni<List<T>> find(Integer offset, Integer limit);

    Uni<List<T>> find(Integer offset, Integer limit, String key, Object value);

    Uni<Boolean> delete(BigInteger id);
}
