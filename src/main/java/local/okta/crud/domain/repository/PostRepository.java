package local.okta.crud.domain.repository;

import java.math.BigInteger;
import java.util.List;

import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.Post;

public interface PostRepository extends CrudRepository<Post> {
    Uni<List<Post>> findMultipleByTagId(Integer offset, Integer limit, BigInteger tagId);

    Uni<Boolean> setTag(BigInteger postId, BigInteger tagId);

    Uni<Boolean> unsetTag(BigInteger postId, BigInteger tagId);
}
