package local.okta.crud.domain.repository;

import java.math.BigInteger;
import java.util.List;

import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.Tag;

public interface TagRepository extends CrudRepository<Tag> {
    Uni<List<Tag>> findMultipleByPostId(Integer offset, Integer limit, BigInteger postId);
}
