package local.okta.crud.delivery.http;

import java.math.BigInteger;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.Post;
import local.okta.crud.domain.entity.QueryRequest;
import local.okta.crud.domain.usecase.CreateOrUpdateTagUseCase;
import local.okta.crud.domain.usecase.DeleteTagUseCase;
import local.okta.crud.domain.usecase.FindMultiplePostsUseCase;
import local.okta.crud.domain.usecase.FindMultipleTagsUseCase;

@Path("/tags")
public class TagPostsController {

    @Inject
    CreateOrUpdateTagUseCase createOrUpdateTagUseCase;

    @Inject
    FindMultiplePostsUseCase findMultiplePostsUseCase;

    @Inject
    DeleteTagUseCase deleteTagUseCase;

    @GET
    @Path("/{tag_id}/posts")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<List<Post>> findMultipleByTagId(@PathParam("tag_id") BigInteger tagId) {
        var queryReq = new QueryRequest();
        queryReq.offset = 0;
        queryReq.limit = 100;
        queryReq.key = "tag_id";
        queryReq.value = tagId;
        
        return findMultiplePostsUseCase.execute(queryReq);
    }
}