package local.okta.crud.delivery.http;

import java.math.BigInteger;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.Post;
import local.okta.crud.domain.entity.QueryRequest;
import local.okta.crud.domain.usecase.CreateOrUpdatePostUseCase;
import local.okta.crud.domain.usecase.DeletePostUseCase;
import local.okta.crud.domain.usecase.FindMultiplePostsUseCase;
import local.okta.crud.domain.usecase.FindOnePostUseCase;
import local.okta.crud.domain.usecase.UseCase;

@Path("/posts")
public class PostsController {

    @Inject
    CreateOrUpdatePostUseCase createOrUpdatePostUseCase;

    @Inject
    FindMultiplePostsUseCase findMultiplePostsUseCase;

    @Inject
    FindOnePostUseCase findOnePostUseCase;

    @Inject
    DeletePostUseCase deletePostUseCase;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Response> create(Post post) {
        if (post == null) {
            throw new BadRequestException("Invalid request body");
        }

        post.id = null;
        return this.createOrUpdatePostUseCase
            .execute(post)
            .map((result) -> Response.status(Status.CREATED).entity(result).build());
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<List<Post>> findMultiple() {
        var queryReq = new QueryRequest();
        queryReq.offset = 0;
        queryReq.limit = 100;

        return this.findMultiplePostsUseCase.execute(queryReq);
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Post> findOne(@PathParam("id") BigInteger id) {
        return this.findOnePostUseCase.execute(id);
            
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Post> update(@PathParam("id") BigInteger id, Post post) {
        if (post == null) {
            throw new BadRequestException("Invalid request body");
        }

        post.id = id;
        return this.createOrUpdatePostUseCase.execute(post);
    }

    @DELETE
    @Path("/{id}")
    public Uni<Void> delete(@PathParam("id") BigInteger id) {
        return this.deletePostUseCase.execute(id);
    }
}