package local.okta.crud.delivery.http;

import java.math.BigInteger;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.PostTag;
import local.okta.crud.domain.entity.QueryRequest;
import local.okta.crud.domain.entity.Tag;
import local.okta.crud.domain.usecase.FindMultipleTagsUseCase;
import local.okta.crud.domain.usecase.SetPostTagUseCase;
import local.okta.crud.domain.usecase.UnsetPostTagUseCase;

@Path("/posts")
public class PostTagsController {


    @Inject
    FindMultipleTagsUseCase findMultipleTagsUseCase;

    @Inject
    UnsetPostTagUseCase unsetPostTagUseCase;

    @Inject
    SetPostTagUseCase setPostTagUseCase;

    @PUT
    @Path("/{post_id}/tags/{tag_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Void> create(
        @PathParam("post_id") BigInteger postId,
        @PathParam("tag_id") BigInteger tagId
    ) {
        return this.setPostTagUseCase.execute(new PostTag(postId, tagId));
    }

    @GET
    @Path("/{post_id}/tags")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<List<Tag>> findMultiple(@PathParam("post_id") BigInteger posId) {
        var queryReq = new QueryRequest();
        queryReq.offset = 0;
        queryReq.limit = 100;
        queryReq.key = "post_id";
        queryReq.value = posId;
        
        return this.findMultipleTagsUseCase.execute(queryReq);
    }

    @DELETE
    @Path("/{post_id}/tags/{tag_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Void> delete(
        @PathParam("post_id") BigInteger postId,
        @PathParam("tag_id") BigInteger tagId
    ) {
        return this.unsetPostTagUseCase.execute(new PostTag(postId, tagId));
    }
}