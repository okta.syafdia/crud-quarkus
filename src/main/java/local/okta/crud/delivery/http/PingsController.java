package local.okta.crud.delivery.http;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.flywaydb.core.Flyway;

import io.smallrye.mutiny.Uni;
import java.time.Duration;

@Path("/ping")
public class PingsController {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Uni<String> ping() {
        return Uni.createFrom().item("pong");
    }
}