package local.okta.crud.delivery.http;

import java.math.BigInteger;
import java.util.List;

import javax.inject.Inject;
import javax.swing.text.html.HTMLDocument.HTMLReader.TagAction;
import javax.validation.Valid;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.smallrye.common.constraint.NotNull;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import local.okta.crud.domain.entity.QueryRequest;
import local.okta.crud.domain.entity.Tag;
import local.okta.crud.domain.usecase.CreateOrUpdateTagUseCase;
import local.okta.crud.domain.usecase.DeleteTagUseCase;
import local.okta.crud.domain.usecase.FindMultipleTagsUseCase;
import local.okta.crud.domain.usecase.FindOneTagUseCase;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("/tags")
public class TagsController {

    @Inject
    CreateOrUpdateTagUseCase createOrUpdateTagUseCase;

    @Inject
    FindMultipleTagsUseCase findMultipleTagsUseCase;

    @Inject
    FindOneTagUseCase findOneTagUseCase;

    @Inject
    DeleteTagUseCase deleteTagUseCase;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Response> create(Tag tag) {
        if (tag == null) {
            throw new BadRequestException("Invalid request body");
        }

        tag.id = null;
        return this.createOrUpdateTagUseCase
            .execute(tag)
            .map((result) -> Response.status(Status.CREATED).entity(result).build());
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<List<Tag>> findMultiple() {
        var queryReq = new QueryRequest();
        queryReq.offset = 0;
        queryReq.limit = 100;

        return this.findMultipleTagsUseCase.execute(queryReq);
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Tag> findOne(@PathParam("id") BigInteger id) {
        return this.findOneTagUseCase.execute(id);
            
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Tag> update(@PathParam("id") BigInteger id, Tag tag) {
        if (tag == null) {
            throw new BadRequestException("Invalid request body");
        }

        tag.id = id;
        return this.createOrUpdateTagUseCase.execute(tag);
    }

    @DELETE
    @Path("/{id}")
    public Uni<Void> delete(@PathParam("id") BigInteger id) {
        return this.deleteTagUseCase.execute(id);
    }
}