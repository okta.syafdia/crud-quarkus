package local.okta.crud.util;

import java.util.NoSuchElementException;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;

import io.vertx.pgclient.PgException;

public class Errors {

    public static Throwable catchError(Throwable t) {
        if (!(t instanceof PgException)) {
            return t;
        }

        if (t instanceof NoSuchElementException) {
            return new NotFoundException("Data is not exist");
        }

        var e = (PgException) t;
        switch (e.getCode()) {
            case "23503":
                return new BadRequestException("Foreign key violation");
            case "23505":
                return new BadRequestException("Data already exists");
            default:
                return e;
        }
    }
    
}
