package local.okta.crud.datasource;

import java.math.BigInteger;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.Tuple;
import local.okta.crud.domain.entity.Tag;
import local.okta.crud.domain.repository.TagRepository;

@ApplicationScoped
public class DbTagRepository extends BaseDbRepository<Tag> implements TagRepository {

    private static final String TABLE_NAME = "tags";

    private static final String COLUMN_LABEL = "label";

    @Override
    public Uni<Tag> create(Tag tag) {
        return this.dbClient
            .preparedQuery(String.format(
                "INSERT INTO %s (%s, %s, %s) VALUES ($1, NOW(), NOW()) RETURNING %s, %s, %s, %s;",
                this.getTableName(),
                COLUMN_LABEL,
                COLUMN_CREATED_AT,
                COLUMN_UPDATED_AT,
                COLUMN_ID,
                COLUMN_LABEL,
                COLUMN_CREATED_AT,
                COLUMN_UPDATED_AT
            ))
            .execute(Tuple.of(tag.label))
            .onItem()
            .transform(rowSet -> this.toEntity(rowSet.iterator().next()));
    }

    @Override
    public Uni<Tag> update(Tag tag) {
        return this.dbClient
            .preparedQuery(String.format(
                "UPDATE %s SET %s = $1, %s = NOW() WHERE %s = $2 RETURNING  %s, %s, %s, %s;",
                this.getTableName(),
                COLUMN_LABEL,
                COLUMN_UPDATED_AT,
                COLUMN_ID,
                COLUMN_ID,
                COLUMN_LABEL,
                COLUMN_CREATED_AT,
                COLUMN_UPDATED_AT
            ))
            .execute(Tuple.of(tag.label, tag.id))
            .onItem()
            .transform(rowSet -> this.toEntity(rowSet.iterator().next()));
    }

    public Uni<List<Tag>> findMultipleByPostId(Integer offset, Integer limit, BigInteger postId) {
        return this.dbClient.preparedQuery(String.join(" ",
                "SELECT t.* FROM tags AS t",
                "JOIN posts_tags AS pt ON pt.tag_id = t.id",
                "WHERE pt.post_id = $1",
                "OFFSET $2 LIMIT $3"
            ))
            .execute(Tuple.of(postId, offset, limit))
            .onItem()
            .transformToMulti((rowSet) -> Multi.createFrom().iterable(rowSet))
            .map(this::toEntity)
            .collect()
            .asList();
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected List<String> getQueryableColumns() {
        return List.of(COLUMN_LABEL);
    }

    @Override
    protected Tag toEntity(Row row) {
        var tag = new Tag();

        tag.id = row.getBigDecimal(COLUMN_ID).toBigInteger();
        tag.label = row.getString(COLUMN_LABEL);
        tag.createdAt = row.getLocalDateTime(COLUMN_CREATED_AT);
        tag.updatedAt = row.getLocalDateTime(COLUMN_UPDATED_AT);

        return tag;
    }
    
}
