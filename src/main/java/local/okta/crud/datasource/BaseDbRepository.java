package local.okta.crud.datasource;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.RowSet;
import io.vertx.mutiny.sqlclient.Tuple;

public abstract class BaseDbRepository<T> {
    @Inject
    PgPool dbClient;

    protected static final String COLUMN_ID = "id";
    protected static final String COLUMN_CREATED_AT = "created_at";
    protected static final String COLUMN_UPDATED_AT = "updated_at";

    public Uni<Boolean> delete(BigInteger id) {
        return this.dbClient
            .preparedQuery(String.format("DELETE FROM %s WHERE id = $1", this.getTableName()))
            .execute(Tuple.of(id))
            .onItem()
            .transform(pgRowSet -> pgRowSet.rowCount() == 1);
    }

    public Uni<List<T>> find(Integer offset, Integer limit) {
        return this.find(offset, limit, null, null);
    }

    public Uni<List<T>> find(Integer offset, Integer limit, String key, Object value) {
        return this.getRowSet(offset, limit, key, value)
            .onItem()
            .transformToMulti((set) -> Multi.createFrom().iterable(set))
            .map(this::toEntity)
            .collect()
            .asList();
    }

    protected abstract String getTableName();

    protected abstract List<String> getQueryableColumns();

    // protected abstract List<String> getCreatableColumns();

    // protected abstract List<String> getUpdateableColumns();

    protected abstract T toEntity(Row row);

    private Uni<RowSet<Row>> getRowSet(Integer offset, Integer limit, String key, Object value) {
        if (key != null && value != null) {
            return this.dbClient
                .preparedQuery(String.format(
                    "SELECT %s FROM %s WHERE %s = $1 ORDER BY created_at OFFSET $2 LIMIT $3;",
                    String.join(",", this.getQueryableColumnsWithDefault()),
                    this.getTableName(),
                    key
                ))
                .execute(Tuple.of(value, offset, limit));

        }

        return this.dbClient
                .preparedQuery(String.format(
                    "SELECT %s FROM %s ORDER BY created_at OFFSET $1 LIMIT $2;",
                    String.join(",", this.getQueryableColumnsWithDefault()),
                    this.getTableName()
                ))
                .execute(Tuple.of(offset, limit));
    }

    private List<String> getQueryableColumnsWithDefault() {
        List<String> results = new ArrayList<>();

        results.addAll(this.getQueryableColumns());
        results.addAll(List.of(
            COLUMN_ID,
            COLUMN_CREATED_AT,
            COLUMN_UPDATED_AT
        ));

        return results;
    }
}
