package local.okta.crud.datasource;

import java.math.BigInteger;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.Tuple;
import local.okta.crud.domain.entity.Post;
import local.okta.crud.domain.repository.PostRepository;

@ApplicationScoped
public class DbPostRepository extends BaseDbRepository<Post> implements PostRepository {

    private static final String TABLE_NAME = "posts";

    private static final String COLUMN_TITLE = "title";
    private static final String COLUMN_CONTENT = "content";

    @Override
    public Uni<Post> create(Post post) {
        return this.dbClient
            .preparedQuery(String.format(
                "INSERT INTO %s (%s, %s, %s, %s) VALUES ($1, $2, NOW(), NOW()) RETURNING %s, %s, %s, %s, %s;",
                this.getTableName(),
                COLUMN_TITLE,
                COLUMN_CONTENT,
                COLUMN_CREATED_AT,
                COLUMN_UPDATED_AT,
                COLUMN_ID,
                COLUMN_TITLE,
                COLUMN_CONTENT,
                COLUMN_CREATED_AT,
                COLUMN_UPDATED_AT
            ))
            .execute(Tuple.of(post.title, post.content))
            .onItem()
            .transform(rowSet -> this.toEntity(rowSet.iterator().next()));
    }

    @Override
    public Uni<Post> update(Post post) {
        return this.dbClient
            .preparedQuery(String.format(
                "UPDATE %s SET %s = $1, %s = $2, %s = NOW() WHERE %s = $3 RETURNING %s, %s, %s, %s, %s;",
                this.getTableName(),
                COLUMN_TITLE,
                COLUMN_CONTENT,
                COLUMN_UPDATED_AT,
                COLUMN_ID,
                COLUMN_ID,
                COLUMN_TITLE,
                COLUMN_CONTENT,
                COLUMN_CREATED_AT,
                COLUMN_UPDATED_AT
            ))
            .execute(Tuple.of(post.title, post.content, post.id))
            .onItem()
            .transform(rowSet -> this.toEntity(rowSet.iterator().next()));
    }

    @Override
    public Uni<List<Post>> findMultipleByTagId(Integer offset, Integer limit, BigInteger tagId) {
        return this.dbClient.preparedQuery(String.join(" ",
                "SELECT p.* FROM posts AS p",
                "JOIN posts_tags AS pt ON pt.post_id = p.id",
                "WHERE pt.tag_id = $1",
                "OFFSET $2 LIMIT $3"
            ))
            .execute(Tuple.of(tagId, offset, limit))
            .onItem()
            .transformToMulti((rowSet) -> Multi.createFrom().iterable(rowSet))
            .map(this::toEntity)
            .collect()
            .asList();
    }

    public Uni<Boolean> setTag(BigInteger postId, BigInteger tagId) {
        return this.dbClient
            .preparedQuery(String.join(" ", 
                "INSERT INTO posts_tags(post_id, tag_id, created_at, updated_at)", 
                "VALUES ($1, $2, NOW(), NOW()) RETURNING id;"
            ))
            .execute(Tuple.of(postId, tagId))
            .onItem()
            .transform(rowSet -> rowSet.rowCount() == 1);
    }

    public Uni<Boolean> unsetTag(BigInteger postId, BigInteger tagId) {
        return this.dbClient
            .preparedQuery("DELETE FROM posts_tags WHERE post_id = $1 AND tag_id = $2;")
            .execute(Tuple.of(postId, tagId))
            .onItem()
            .transform(rowSet -> rowSet.rowCount() == 1);
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected List<String> getQueryableColumns() {
        return List.of(COLUMN_TITLE, COLUMN_CONTENT);
    }

    @Override
    protected Post toEntity(Row row) {
        var post = new Post();

        post.id = row.getBigDecimal(COLUMN_ID).toBigInteger();
        post.title = row.getString(COLUMN_TITLE);
        post.content = row.getString(COLUMN_CONTENT);
        post.createdAt = row.getLocalDateTime(COLUMN_CREATED_AT);
        post.updatedAt = row.getLocalDateTime(COLUMN_UPDATED_AT);

        return post;
    }
    
}
